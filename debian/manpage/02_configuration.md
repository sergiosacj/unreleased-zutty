CONFIGURATION
=============

Zutty has a set of configuration options, all of which have:

* a command line option;
* an X resource database (Xrdb) key;
* a sensible hard default.

For each option, the above list defines the order of preference. That is, the
command line can be used to define or override a setting in a transient way;
Xrdb entries may be used to persistently alter the default value for an option;
and lastly, without any option or configuration, Zutty will still have a
workable default.

There is also a set of __EXTRA RESOURCES__ available for persistent configuration,
but not settable via command line options.

